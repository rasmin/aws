#!/usr/bin/env node

const cdk = require('@aws-cdk/core');
const { HelloWorldStack } = require('../lib/hello-world-stack');

const app = new cdk.App();
new HelloWorldStack(app, 'HelloWorldStack');
